require 'rails_helper'

describe Admin::ApplicationHelper, type: :helper do
  describe '#timestamp' do
    it "should return 'Never' if a timedate is not given" do
      expect(timestamp).to eq('Never')
    end

    it "should reutrn 'x ago' when the supplied timestamp is less than a week ago" do
      expect(timestamp(6.days.ago)).to match('.*? ago')
    end

    it "should not return 'x at x' when the timestamp is less than a week ago" do
      expect(timestamp(5.minutes.ago)).to_not match(/.*? at .*/)
    end

    it "should return 'x at x' when the timestamp is more than a week ago" do
      expect(timestamp(7.days.ago)).to match(/.*? at .*/)
    end

    it "should not reutrn 'x ago' when the supplied timestamp is more than a week ago" do
      expect(timestamp(8.days.ago)).to_not match('.*? ago')
    end

  end

  describe '#tb_form_errors' do
    before(:each) do
      @user = FactoryBot.create(:spud_user)
    end

    it 'should return blank when no errors exist' do
      expect(helper.tb_form_errors(@user)).to be_blank
    end

    it 'should display the error' do
      error = 'Invalid!'
      @user.errors[:base] << error
      expect(helper.tb_form_errors(@user)).to include(error)
    end
  end
end
