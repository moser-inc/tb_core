//= require jquery
//= require tb_core
//= require bootstrap-sprockets
//= require_self
//= require_directory .

(function(){

window.app = {
  init: function(){
    // global initializer
  }
};

document.addEventListener('DOMContentLoaded', app.init);

})();
