require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require
require 'tb_core'

module Dummy
  class Application < Rails::Application
    TbCore.configure do |config|
      config.site_name = 'Dummy'
      config.from_address = 'no-reply@dummy.com'
    end

    config.load_defaults '6.0'
  end
end
