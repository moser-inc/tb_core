require 'rails_helper'
require 'rspec/mocks'
require 'rspec/mocks/standalone'

describe TbCore::ApplicationController, type: :controller do

  describe 'not_found' do
    it 'throws a 404 error' do
      get :not_found
      expect(response.code).to eq('404')
      expect(response).to render_template('layouts/error_page')
    end
  end

end
