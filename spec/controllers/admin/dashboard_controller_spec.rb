require 'rails_helper'

RSpec.describe Admin::DashboardController, type: :controller do

  before(:all) do
    TbCore.admin_applications += [{
      name: 'Test App',
      url: '/test/app',
      permissions: ['admin.test_app.full_access']
    },
    {
      name: 'dash app',
      key: 'dash_app',
      badge: ->(user){ 1 }
    }]
  end

  before(:each) do
    activate_session(admin: true)
  end

  describe 'index' do
    it 'should display applications the current user has access to given that the current user is not a super admin' do
      @user.super_admin = false
      @user.role = FactoryBot.create(:spud_role, permission_tags: ['admin.users.full_access'])
      @user.save()
      get :index

      expect(assigns(:admin_applications).collect{|app| app[:name] }).to include('Users')
    end

    it 'should not display applications the current user does not have access
      to given that the current user is not a super admin' do
      @user.super_admin = false
      @user.role = FactoryBot.create(:spud_role, permission_tags: ['admin.users.full_access'])
      @user.save()
      get :index

      expect(assigns(:admin_applications).collect{|app| app[:name] }).to_not include('Test App')
    end

    it 'should display all the applications despite the users permissions given
      the current user is a super admin' do
      @user.super_admin = true
      @user.role = nil
      @user.save
      get :index

      excectation = TbCore.admin_applications.collect{|app| app[:name] }
      expect(assigns(:admin_applications).collect{|app| app[:name] }).to eq(excectation)
    end
  end

  describe 'badges' do
    before(:each) do
      request.accept = 'application/json'
      TbCore.configure do |config|
        config.site_name = 'Test Site'
      end
      @user.super_admin = true
      @user.save
    end

    it 'should respond with a json content type' do
      get :badges

    end

    it 'should contain data array in reponse' do
      get :badges
      json = JSON.parse(response.body)
      expect(response.media_type).to eq('application/json')
      expect(json).to have_key('data')
    end

    it 'should have badge count 1 for dash app' do
      get :badges
      json = JSON.parse(response.body)
      data = json['data']
      assert data.size  == 1
      app_found = false
      badge_count = 0
      data.each do |app|
        if app['key'] == 'dash_app'
          app_found = true
          badge_count = app['badge_count']
        end
      end
      assert app_found && badge_count == 1
    end

  end
end
