require 'rails_helper'

describe Admin::UsersController, type: :controller do

  before(:each) do
    activate_session(admin: true)
    @role = FactoryBot.create(:spud_role)
    TbCore.admin_applications += [{name: 'Test', key: :test}]
    TbCore.permissions.push(SpudPermission.new('admin.test.full_access', 'Test', [:test]))
  end

  describe 'index' do
    it 'should return an array of users' do
      2.times {|x| FactoryBot.create(:spud_user) }
      get :index

      expect(assigns(:spud_users).count).to be > 1
    end

    it 'should not return any users if there are no users' do
      get :index

      expect(assigns(:spud_users).count).to eq(1) # the currently logged in user is the only user
    end

    it 'should not allow access to users with NO permissions' do
      SpudUserSession.create(FactoryBot.build(:spud_user, super_admin: false))
      get :index

      expect(response.code).to eq('403')
      expect(response).to render_template('layouts/admin/error_page')
    end

    it 'should allow access to users with the correct permissions' do
      u = FactoryBot.create(:spud_user, super_admin: false)
      @role.permission_tags = ['admin.users.full_access']
      @role.save()
      u.role = @role
      SpudUserSession.create(u)
      get :index

      expect(response).to be_successful
    end

    it 'should not allow access to users without a role,
      and redirect to render error page if the user has no permissions' do
      u = FactoryBot.create(:spud_user, super_admin: false)
      u.role = nil
      SpudUserSession.create(u)
      get :index

      expect(response.code).to eq('403')
      expect(response).to render_template('layouts/admin/error_page')
    end

    it 'should not allow access to users with a role that contains no permissions,
      and render error page if the users has no other admin modules' do
      u = FactoryBot.create(:spud_user, super_admin: false)
      u.role = @role
      @role.spud_role_permissions = []
      SpudUserSession.create(u)
      get :index

      expect(response.code).to eq('403')
      expect(response).to render_template('layouts/admin/error_page')
    end

    it 'should not allow access to users without permission
      and render error page if the users has other admin modules' do
      u = FactoryBot.create(:spud_user, super_admin: false)
      @role.permission_tags = ['admin.test.full_access']
      u.role = @role
      SpudUserSession.create(u)
      get :index

      expect(response.code).to eq('403')
      expect(response).to render_template('layouts/admin/error_page')
    end
  end

  describe 'show' do
    it 'should respond successfully' do
      user = FactoryBot.create(:spud_user)
      get :show, params: { id: user.id }
      expect(response).to be_successful
    end
  end

  describe 'new' do
    it 'should render the form' do
      get :new, format: :html
      expect(response).to be_successful
    end
  end

  describe 'create' do
    context 'HTML format' do
      it 'should create a new user with a valid form submission' do
        expect {
          post :create, params: { spud_user: FactoryBot.attributes_for(:spud_user) }
        }.to change(SpudUser, :count).by(1)
      end

      it 'should not create a user with an invalid form entry' do
        expect {
          post :create, params: { spud_user: FactoryBot.attributes_for(:spud_user, email: nil) }
        }.to_not change(SpudUser, :count)
      end
    end
  end

  describe 'edit' do
    context 'HTML format' do
      it 'should load the correct user for the edit form' do
        user = FactoryBot.create(:spud_user)
        get :edit, params: { id: user.id }

        expect(assigns(:user).id).to eq(user.id)
      end
    end
  end

  describe 'update' do
    it 'should update the email when the first name attribute is changed' do
      user = FactoryBot.create(:spud_user)
      new_name = 'Adam'
      expect {
        put :update, params: { id: user.id, spud_user: { first_name: new_name } }
        user.reload
      }.to change(user, :first_name).to(new_name)
    end

    it 'should redirect to the admin users show view after a successful update' do
      user = FactoryBot.create(:spud_user)
      put :update, params: { id: user.id, spud_user: user.attributes.merge!(first_name: 'Adam') }

      expect(response).to redirect_to(admin_user_path(user))
    end
  end

  describe 'destroy' do
    it 'should destroy the user' do
      user = FactoryBot.create(:spud_user)
      expect {
        delete :destroy, params: { id: user.id }
      }.to change(SpudUser, :count).by(-1)
      expect(response).to be_redirect
    end

    it 'should destroy the user with the wrong id' do
      FactoryBot.create(:spud_user)
      expect {
        delete :destroy, params: { id: '23532' }
      }.to_not change(SpudUser, :count)
    end
  end
end
