require 'rails_helper'

describe Admin::SettingsController, type: :controller do
  before(:each) do
    activate_session(admin: true)
  end

  describe 'edit' do
    it 'should respond with success' do
      get :edit
      expect(response).to be_successful
    end
  end

  describe 'update' do
    it 'should give an error if password change does not include a valid current password' do
      u = FactoryBot.build(:spud_user)
      u.password = 'test'
      put :update, params: { spud_user: {first_name: 'Mike', password: 'Imapassword', current_password: 'ImaTest'} }
      expect(response).to have_http_status(401)
    end

    it 'should redirect to the settings page with a successful update' do
      put :update, params: { spud_user: {first_name: 'Mike'} }
      expect(response).to redirect_to(admin_settings_path)
    end

    it 'should put a message in the flash notice' do
      put :update, params: { spud_user: {first_name: 'Mike'} }
      expect(flash[:notice]).to_not be_blank
    end
  end

end
