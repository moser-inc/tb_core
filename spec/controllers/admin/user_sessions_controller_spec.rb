require 'rails_helper'

describe Admin::UserSessionsController, type: :controller do
  before(:each) do
    # activate_authlogic
    # SpudUserSession.create(FactoryBot.build(:spud_user))
  end

  describe 'new' do
    it 'should redirect to setup url if no users exist' do
      get :new
      expect(response).to redirect_to(admin_setup_path)
    end

    it 'should render login form if users do exist' do
      u = FactoryBot.create(:spud_user)
      u.save
      get :new
      expect(response).to be_successful
    end
  end

end
