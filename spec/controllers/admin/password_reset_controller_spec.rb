require 'rails_helper'

describe Admin::PasswordResetsController, type: :controller do

  let(:user) { FactoryBot.build(:spud_user, id: 1, perishable_token: 'jfdlsafhbkvabuadfbds') }

  context 'get' do
    describe 'index' do
      it 'should return success' do
        get :index
        expect(response).to be_successful
      end
    end

    describe 'show' do
      context 'with a valid id' do
        it 'should render the edit form' do
          allow(SpudUser).to receive(:find_using_perishable_token).and_return(user)
          get :show, params: { id: 1 }
          expect(response).to be_successful
        end
      end

      context 'with an invalid id' do
        it 'should redirect to the login form' do
          allow(SpudUser).to receive(:find_using_perishable_token).and_return(nil)
          get :show, params: { id: user.id }
          expect(response).to redirect_to(admin_login_path)
        end
      end
    end
  end

  context 'post' do

    describe 'create' do
      context 'with a valid user email address submitted' do
        it 'should trigger the password notificiation' do
          allow(SpudUser).to receive(:find_by).and_return(user)
          expect do
            post :create, params: { email: user.email }
          end.to have_enqueued_job.on_queue('mailers')
        end

        it 'should redirect to the login form' do
          allow(SpudUser).to receive(:find_by).and_return(user)

          post :create, params: { email: user.email }
          expect(response).to redirect_to(admin_login_path)
        end
      end

      context 'with an invalid user email address submitted' do
        before(:each) do
          allow(SpudUser).to receive(:find_by).and_return(nil)
        end
        it 'should re-render the password reset form' do
          post :create, params: { email: 'invalid@email.com' }
          expect(response).to render_template('index')
        end
      end
    end

    describe 'update' do
      let(:valid_data) { {id: user.id, spud_user: {password: 'password', password_confirmation: 'password'}}}
      let(:invalid_data) { {id: user.id, spud_user: {password: 'password', password_confirmation: 'drowssap'}}}

      before(:each) do
        allow(SpudUser).to receive(:find_using_perishable_token).and_return(user)
      end

      context 'with valid password entry' do
        it 'should save and redirect to the login form' do
          post :update, params: valid_data
          expect(response).to redirect_to(admin_login_path)
        end
      end

      context 'with an invalid password entry' do
        it 'should re-render the password form' do
          post :update, params: invalid_data
          expect(response).to render_template('show')
        end
      end
    end
  end
end
