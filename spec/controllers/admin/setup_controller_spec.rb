require 'rails_helper'

describe Admin::SetupController, type: :controller do

  describe 'new' do
    it 'should be successful' do
      get :new

      expect(response).to be_successful
    end

    it 'should redirect to the admin login form when there is already a user' do
      allow(SpudUser).to receive(:count).and_return(1)
      get :new

      expect(response).to redirect_to(admin_login_path)
    end
  end

  describe 'create' do
    it 'should create a new user' do
      expect{
        post :create, params: { spud_user: FactoryBot.attributes_for(:spud_user) }
      }.to change(SpudUser, :count).by(1)
    end

    it 'should redirect to the admin dashboard when first admin user has been created' do
      post :create, params: { spud_user: FactoryBot.attributes_for(:spud_user) }
      expect(response).to redirect_to(admin_root_path)
    end
  end
end
