require 'rails_helper'

RSpec.describe Admin::ApplicationController, type: :controller do

  before :each do
    activate_authlogic
    @user = FactoryBot.create(:spud_user)
    @role = FactoryBot.create(:spud_role)
    @session = SpudUserSession.create(@user)
  end

  describe 'require_user' do
    controller(Admin::ApplicationController) do
      def index
        head :ok
      end
    end

    it 'should respond successfully if the current user is a super admin' do
      @user.update(super_admin: true)
      get :index
      expect(response).to be_successful
    end

    it 'should respond successfully if the current user has admin permissions' do
      @role.permission_tags = ['admin.users.full_access']
      @role.save
      @user.role = @role
      @user.save
      get :index
      expect(response).to be_successful
    end

    it 'should redirect to the login if the current user is not logged in' do
      @session.destroy
      get :index
      expect(response).to redirect_to(admin_login_path(return_to: '/admin/application'))
    end

    it 'should redirect to the root for a user without administrative priviledges' do
      @user.super_admin = false
      @user.role = nil
      @user.save
      get :index
      expect(response.code).to eq('403')
      expect(response).to render_template('layouts/admin/error_page')
    end
  end
end
