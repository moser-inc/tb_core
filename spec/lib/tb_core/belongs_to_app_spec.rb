require 'rails_helper'

class DummyController < ActionController::Base
  include TbCore::BelongsToApp
  attr_accessor :current_user
end

describe TbCore::BelongsToApp do

  let(:controller){
    DummyController.new()
  }

  before(:each) do

  end

  describe '.belongs_to_app' do
    it 'should add a before_action' do
      # Not sure how to test for this...
      DummyController.belongs_to_app(:users)
    end
  end

  describe '#acts_as_app' do
    it 'should configure the app' do
      controller.current_user = FactoryBot.create(:spud_user, super_admin: true)
      controller.send(:act_as_app, :users)
      expect(controller.instance_variable_get('@page_application')).to be_a(Hash)
    end

    it 'should raise an error if the app cannot be found' do
      expect{
        controller.send(:act_as_app, :fail)
      }.to raise_error(StandardError)
    end

    it 'should raise an error if the user does not have access' do
      controller.current_user = FactoryBot.create(:spud_user, super_admin: false)
      expect{
        controller.send(:act_as_app, :users)
      }.to raise_error(TbCore::AccessDeniedError)
    end
  end

  describe '#determine_page_name' do
    it 'should contain "New"' do
      result = controller.send(:determine_page_name, 'Users', 'new')
      expect(result).to eq('New User')
    end

    it 'should contain "Edit"' do
      result = controller.send(:determine_page_name, 'Users', 'edit')
      expect(result).to eq('Edit User')
    end

    it 'should contain "Detail"' do
      result = controller.send(:determine_page_name, 'Users', 'show')
      expect(result).to eq('User Detail')
    end

    it 'should return the base name' do
      result = controller.send(:determine_page_name, 'Users', 'other')
      expect(result).to eq('Users')
    end
  end

end
