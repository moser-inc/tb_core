# Change Log

This log will detail all major releases of TB Core. Minor patch updates may be omitted if they are simple bug fixes.

## v1.3.10

- Change the structure of json formatted errors to be more predictable

## v1.3.7

- TinyMCE is now being lazy-loaded via a CDN instead of being packaged in through the asset pipeline.

## 1.3.5

- Dashboard modules are now sortable by the user via drag-and-drop #[5](https://bitbucket.org/westlakedesign/tb_core/pull-requests/5)
- A long-standing annoyanced was addressed when running migrations under utf8mb4 encoding #[6](https://bitbucket.org/westlakedesign/tb_core/pull-requests/6)
- Further refinements to the remote form handler javascript

## 1.3.4

- Make it easy to embed a login form on any page using the `user_session/form` partial.
- Add `tb_user_select` helper to the `TbCore::FormBuilder` class, which allows assigning or creating a new user from another form.
- Add a couple new options to the user form, including generated passwords and requiring password changes on login.
- Better support for customizing your twitter bootstrap assets.
- Better looking core email templates
- Replaced local jquery-ui library with the `jquery-ui-rails` ruby gem

## 1.3.3

- This update makes heavy usage of the jquery-ujs gem and it's remote form tools.

## 1.2.8

- Add ability for login form to specify a `return_to` param on login and password reset forms

## 1.2.5

- Update Tinymce editor to version 4.x
- Many improvements to page request error handling

## 1.2.4

- Add basic fragment caching code to module generator
- Add a `tb_core_tabbed_navigation` helper for building twitter bootstrap tabbed navigations in the admin
- Misc fixes

## 1.2.3

- Code clean up and removal of deprecated functions
- Fix 404 handler on images and other formats

## 1.2.2

- Admin modules can now display an iOS-style badge count on their dashboard icons
- Adds `tb_core` js file that apps can optionally require via asset pipeline
- Improvements to setup and module generators
- Miscellaenous bug ixes

## 1.2.1

This update pulls in the latest Authlogic, which should be fully compatible with Rails 4.

## 1.2.0

Twice Baked is now running Rails 4! For Rails 3 compatibility, please continue to use version 1.1.x.

Other changes:

- Improvements to managing and extending the `SpudUser` model.
- Replace our copy of tinymce with the `tinymce-rails` gem.

## v1.1.10

- Added `Spud::Core.config.production_alert_domain` option. When set, displays a prominent warning in the admin directing users to their production website.
- Allows `Spud::NotFoundError` exceptions to specify a custom layout to render.
- `Spud::Setup` generator will attempt to replace nasty `require_tree` directives with `require_directory`.

## v1.1.7

- Added a setup generator for base controller & layouts

## v1.1.6

- Generators! Incorporated the handy module generator from `spud_core_admin` for easy dashboard module creation.

## 1.1.5

- Added new exception `Spud::NotFoundError` and rescue hook that renders a 404 template.

## v1.1.0

Revamped User admin module, now with Roles and a more flexible Permission system. See readme for instructions.

**WARNING**: Migrating from 1.0 will erase any permission settings you currently have. A permission migration script should be forthcoming.

*Also in this release:*

- Refactored user login flow to differentiate between Admin and non-Admin login templates
- Added a SplitPane javascript widget for use in your own admin apps
- Mass simplification of namespacing throughout the engine *(this is an ongoing effort)*

## v1.0.0

Minimal changes to admin UI. Mostly just a version bump to inaugurate the release of TB Core.

## v0.9.17

First release as Twice Baked.
