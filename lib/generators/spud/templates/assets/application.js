//= require rails-ujs
//= require rails-validator
//= require_self
//= require_directory .

(function(){

window.app = {
  init: function(){
    // global initializer
  }
};

document.addEventListener('DOMContentLoaded', app.init);

})();
