class Spud::SetupGenerator < ::Rails::Generators::Base
  desc 'Base setup for a new TB application'
  source_root File.expand_path('templates', __dir__)

  def setup
    if ask(
      'NOTICE: Setup will now attempt to delete and/or overwrite existing files. Continue?',
      limited_to: ['y', 'n']
    ) == 'n'
      return
    end

    install_templates()
    install_bootstrap() if defined?(Bootstrap)

    environment(spud_core_configs())
    rake('railties:install:migrations')

    rake('db:migrate') if ask('Migrate the database?', limited_to: ['y', 'n']).downcase == 'y'
  end

private

  def install_templates
    template 'views/layouts/application.html.erb', 'app/views/layouts/application.html.erb'
    template 'views/layouts/error_page.html.erb', 'app/views/layouts/error_page.html.erb'
    template 'application_controller.rb', 'app/controllers/application_controller.rb'
    template 'assets/admin.scss', 'app/assets/stylesheets/admin/application.scss'
    remove_file 'app/assets/stylesheets/application.css'
    template 'assets/application.scss', 'app/assets/stylesheets/application.scss'
    template 'assets/application.js', 'app/assets/javascripts/application.js'
    template 'assets/admin/application.js', 'app/assets/javascripts/admin/application.js'
  end

  def install_bootstrap
    theme_file = Bootstrap::Rails::Engine.root.join('templates', 'project', '_bootstrap-variables.sass')
    template theme_file, 'app/assets/stylesheets/imports/bootstrap_theme.scss' if File.exist?(theme_file)
    modules_file = Bootstrap::Rails::Engine.root.join('assets', 'stylesheets', '_bootstrap.scss')
    template modules_file, 'app/assets/stylesheets/imports/bootstrap_modules.scss' if File.exist?(modules_file)
  end

  def spud_core_configs
    site_name = application_name().titleize
    domain_name = application_name().dasherize
    return <<~RUBY
      TbCore.configure do |config|
            config.site_name = "#{site_name}"
            config.from_address = "no-reply@#{domain_name}.com"
          end
    RUBY
  end

  def application_name
    Rails.application.class.name.split('::').first.underscore
  end

end
