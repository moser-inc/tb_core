class Spud::ControllerSpecGenerator < ::Rails::Generators::Base
  source_root File.expand_path('templates', __dir__)

  argument :name, type: :string
  argument :string_attribute, type: :string
  argument :editable, type: :numeric, default: 0

  def controller
    template 'controller_spec.rb.erb', "spec/controllers/#{name.downcase}_controller_spec.rb"
  end

private

  def controller_class_name
    return name.camelize + 'Controller'
  end

  def model_variable_name
    return name.split('/').last.downcase.singularize
  end

  def model_class_name
    return name.split('/').last.camelize.singularize
  end

  def model_string_attribute_name
    return string_attribute.downcase
  end

  def include_edit_actions?
    return editable == 1
  end

  def include_user_session?
    return name.match('Admin/')
  end
end
