module TbCore

  class RequestError < StandardError
    attr_accessor :request_url, :item, :template
    attr_reader :code, :title

    # For compatability reasons, this method accepts multiple styles of inputs
    # Going forward the expected input will be:
    #
    # * item: The item that could not be found. String. (default = page)
    # * template (named): ERB template you wish to render
    #
    def initialize(item_or_opts='page', opts={})
      if item_or_opts.is_a?(Hash)
        @item = item_or_opts[:item]
        @template = item_or_opts[:template]
      else
        @item = item_or_opts
        @template = opts[:template]
      end
      @template ||= 'layouts/error_page'
      @title = I18n.t(:title, scope: [:tb_core, :errors, @i18n])
      super(I18n.t(:message, scope: [:tb_core, :errors, @i18n], item: @item))
    end
  end

  class AccessDeniedError < RequestError
    def initialize(item_or_opts='page', opts={})
      @code = 403
      @i18n = 'access_denied'
      super(item_or_opts, opts)
    end
  end

  class NotFoundError < RequestError
    def initialize(item_or_opts='page', opts={})
      @code = 404
      @i18n = 'not_found'
      super(item_or_opts, opts)
    end
  end

  class UnauthorizedError < RequestError
    def initialize(item_or_opts='page', opts={})
      @code = 401
      @i18n = 'unauthorized'
      super(item_or_opts, opts)
    end
  end
end
