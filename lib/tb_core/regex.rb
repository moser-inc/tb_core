module TbCore
  module Regex
    EMAIL = /
      \A
      [A-Z0-9_.&%+\-']+   # mailbox
      @
      (?:[A-Z0-9\-]+\.)+  # subdomains
      (?:[A-Z]{2,25})     # TLD
      \z
    /ix.freeze
  end
end
