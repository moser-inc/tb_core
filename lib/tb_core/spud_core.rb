module SpudDeprecatedNamespace
  def const_missing(const)
    ActiveSupport::Deprecation.warn(
      'The Spud namespace is being replaced by TbCore. Please update your app code accordingly.',
      caller
    )
    TbCore.const_get(const)
  end

  def method_missing(method_signature, *args, &block)
    if TbCore.respond_to?(method_signature)
      ActiveSupport::Deprecation.warn(
        'The Spud::Core namespace is being replaced by TbCore. Please update your app code accordingly.',
        caller
      )
      TbCore.__send__(method_signature, *args, &block)
    else
      super
    end
  end

  def respond_to_missing?(method_signature)
    TbCore.respond_to?(method_signature) || super
  end
end

module Spud
  extend SpudDeprecatedNamespace
end

module Spud::Core
  extend SpudDeprecatedNamespace
end
