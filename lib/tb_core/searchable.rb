module Spud::Searchable
  extend ActiveSupport::Concern
  included do
    extend ClassMethods
  end
  module ClassMethods
    def spud_searchable
      ActiveSupport::Deprecation.warn(
        'Spud::Searchable is deprecated and will be removed from future releases.',
        caller
      )
    end
  end
end
