module TbCore
  module BelongsToApp
    extend ActiveSupport::Concern

    module ClassMethods
      def belongs_to_spud_app(name, options={})
        ActiveSupport::Deprecation.warn(
          'ApplicationController#belongs_to_spud_app is deprecated. Please use #belongs_to_app instead.',
          caller
        )
        belongs_to_app(name, page_title: options[:page_title])
      end

      # Adds a before action to the current controller to act like the specified twice baked app
      #
      # symbol: This should be the :key value for the app you want to use
      # page_title: Override the base name value for the app (optional)
      # only: An array of controller actions you want this to apply to (optional)
      #
      def belongs_to_app(name, page_title: nil, only: nil)
        before_action ->{
          act_as_app(name, page_title: page_title)
        }, only: only
      end
    end

  private

    # Call the make the current controller behave as though it belongs to the specified twice baked app
    #
    # symbol: This should be the :key value for the app you want to use
    # page_title: Override the base name value for the app (optional)
    #
    def act_as_app(symbol, page_title: nil)
      @page_application = TbCore.admin_applications.find{ |app| app[:key] == symbol }
      if @page_application.blank?
        raise "Requested application '#{symbol}' could not be found"
      elsif !current_user.can_view_app?(@page_application)
        raise AccessDeniedError.new(item: 'module', template: '/layouts/admin/error_page')
      end

      @page_thumbnail = @page_application[:thumbnail]
      @page_name = determine_page_name(page_title || @page_application[:name], action_name)
    end

    # Takes a base app name and controller action name and returns an appropriate page title
    #
    def determine_page_name(base_name, action_name)
      if ['new', 'create'].include?(action_name)
        return "New #{base_name.singularize}"
      elsif ['edit', 'update'].include?(action_name)
        return "Edit #{base_name.singularize}"
      elsif action_name == 'show'
        return "#{base_name.singularize} Detail"
      else
        return base_name
      end
    end
  end
end
