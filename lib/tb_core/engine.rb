require 'responders'
require 'jquery-rails'
require 'authlogic'
require 'bootstrap-sass'
require 'will_paginate'
require 'will_paginate-bootstrap'
require 'breadcrumbs_on_rails'
require 'rails-ujs'
require 'sortable_by'
require 'tinymce-rails'

module Spud
end

module TbCore
  class Engine < ::Rails::Engine
    require "#{root}/lib/tb_core/errors"
    require "#{root}/lib/tb_core/form_builder"
    require "#{root}/lib/tb_core/regex"
    require "#{root}/lib/tb_core/searchable"
    require "#{root}/lib/tb_core/spud_core"

    engine_name :tb_core
    config.autoload_paths << "#{root}/lib"

    initializer 'tb_core.admin', after: :admin do
      # Translate Hash configured permissions into SpudPermission objects
      TbCore.permissions.collect! do |p|
        if p.is_a? Hash
          SpudPermission.new(p[:tag], p[:name], p[:apps])
        else
          p
        end
      end

      # Append Users admin module
      TbCore.config.admin_applications.unshift(name: 'Users',
                                               thumbnail: 'admin/users_thumb.png',
                                               url: '/admin/users',
                                               order: 100)

      # Create default permissions for modules
      TbCore.admin_applications.each do |admin_application|
        admin_application[:key] ||= admin_application[:name].tr(' ', '_').downcase.to_sym
        tag = "admin.#{admin_application[:key]}.full_access"
        name = "#{admin_application[:name]} admin"
        apps = [admin_application[:key]]
        TbCore.permissions.push(SpudPermission.new(tag, name, apps))
      end
    end

    initializer 'tb_core.catch_all_route' do |config|
      # Handle 404 errors if Spud::Cms is not installed
      config.routes_reloader.paths << File.expand_path('catch_all_route.rb', __dir__) unless defined?(Spud::Cms)
    end

    initializer 'tb_core.assets' do
      Rails.application.config.assets.precompile += ['admin/users_thumb.png', 'admin/module_icon.png']
    end
  end
end
