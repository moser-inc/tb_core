class TbCore::Responder < ActionController::Responder

  def initialize(*)
    super

    # Don't require a :location parameter for redirecting
    if !@options.key?(:location)
      @options[:location] = nil
    end
  end
end
