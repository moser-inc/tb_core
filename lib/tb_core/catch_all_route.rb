Rails.application.routes.draw do
  match '*any' => 'tb_core/application#not_found', via: :all
end
