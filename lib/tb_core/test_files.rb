module Spud
  module Core
    module TestFiles
      class << self
        def load_all
          load_specs
          load_factories
          load_support
        end
        
        def load_specs
          Dir[File.join(File.expand_path('../..', __dir__), 'spec/**/*_spec.rb')].each {|f| require f}
        end
          
        def load_factories
          Dir[File.join(File.expand_path('../..', __dir__), 'factories/*')].each {|f| require f}
        end
        
        def load_support
          Dir[File.join(File.expand_path('../..', __dir__), 'spec/support/**/*.rb')].each {|f| require f}
        end
      end
    end
  end
end
