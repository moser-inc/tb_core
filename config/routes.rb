Rails.application.routes.draw do

  namespace :admin do

    get 'login' => 'user_sessions#new', :as => 'login'
    get 'badges' => 'dashboard#badges'

    resources :password_resets, :only => [:index, :create, :show, :update], :path => 'login/forgot'

    get 'settings' => 'settings#edit', :as => 'settings'
    put 'settings' => 'settings#update'
    put 'change_sort' => 'dashboard#change_sort'

    root :to => "dashboard#index", as: :root
    resources :users do
      get 'export', :on => :collection
    end
    resources :roles

    get 'setup' => 'setup#new'
    post 'setup' => 'setup#create'
  end

  # Non-admin login paths
  get 'login' => 'user_sessions#new', :as => 'login'
  post 'login' => 'user_sessions#create'
  match 'logout' => 'user_sessions#destroy', :as => 'logout', :via => [:get, :post]

  get 'login/change_password' => 'user_sessions#change_password'
  post 'login/change_password' => 'user_sessions#set_change_password'

  resources :password_resets, :only => [:index, :create, :show, :update], :path => 'login/forgot'

  get 'spud/admin' => 'admin/user_sessions#legacy_redirect'
  get 'tb_core/not_found' => 'tb_core/application#not_found', as: 'not_found' unless Rails.env.production?

end
