[![CircleCI](https://circleci.com/bb/moser-inc/tb_core.svg?style=svg)](https://circleci.com/bb/moser-inc/tb_core)

Twice Baked Core
================

TB Core is the base Twice Baked engine that provides user authentication, roles, and an admin dashboard upon which CRUD apps can be built. Use TB Core to power content-managed websites or as a quick-and-simple way to add a backend to your existing data models.

Requirements
-----------

TB Core 1.2.x and higher requires Rails 4 and is developed using Ruby 2.1. Lower versions of Ruby may continue to work but are not heavily tested.

For Rails 3 compatiblity, stick to TB Core 1.1.x.

Installation/Usage
------------------

1. In your Gemfile add the following

        gem 'tb_core'

2. Run bundle install

3. Run the setup generator to copy in the base templates and migrations

        rails g spud:setup

4. run a rails server instance and point your browser to /admin

Setup Tasks
-----------

The `spud:setup` generator takes care of a few tasks for you. If you missed that step or would like to complete them manually, they are listed below:

1. Your base `ApplicationController` should extend from `TbCore::ApplicationController`.
2. Your base `application.html.erb` should include a few special head tags (see generator template for example).
3. You should copy in the base migrations with `rake railties:install:migrations`.

Configuration
-------------

The core engine provides various configuration options.

    TbCore.configure do |config|
      config.option = value
      ...
    end

- `admin_applications`: Array of custom admin modules. See section below for more info.
- `site_name`: Controls the site name displayed in the dashboard and `tb_page_title` view helper.
- `from_address`: Sender address used for password resets and other mailers.
- `production_alert_domain`: When set, displays a prominent warning bar in the admin dashboard directing users to the production domain. You should only set this value in `config/environments/staging.rb`.

Adding Apps to the Dashboard
----------------------------

### Using the Module Generator

The fastest way to add an app to the dashboard is via the geneator. The generator is best suited for situations where you are also creating a new ActiveRecord model, and you want to create a standard set of CRUD actions to manage it.

    rails g spud:module Cars make:string model:string year:integer classic:boolean notes:text

Running the example above would create an ActiveRecord model, migration, admin controller with CRUD actions, and a set of front end views.

### Adding Manually

Sometimes you want to create a dashboard app without all the magic described above. Start by building your controllers and views under the `admin` namespace. Extend your controller from `Admin::ApplicationController` in order to inherit the default admin behaviors, and look at the core admin controllers for example implementations.

Then, add your module to the `TbCore.config.admin_applications` array. We recommend you perform this action in either an initializer or within your `application.rb` file.

    TbCore.config.admin_applications += [{
      # Required:
      :name => "Clients",
      :thumbnail => "spud/admin/clients.png",
      :url => "/admin/clients",
      # Optional:
      :badge => ->(user){ Client.where(:is_pending => true).count() }
    }]

Restart your application, and the new module should appear on the dashboard.

Extending the User Model
------------------------

A common requirement is to add custom fields and functionality to the base `spud_user` model. We have provided several spots where you can do this.

### Adding custom methods and attributes to the SpudUser class

Create a file in your app at `app/models/spud_user.rb`, and build a class called `SpudUser` that includes the `TbCore::UserModel` module.

    class SpudUser < ApplicationRecord
      include TbCore::UserModel
      has_attached_file :avatar
      attr_accessible :avatar
      def say_hello
        return "Hello, World!"
      end
    end

### Adding fields to the add/edit user form

Create a file in your app at `app/views/admin/users/_form_additions.html.erb`.

    <div class="control-group">
      <%= f.label :avatar, :class=>"control-label"%>
      <div class="controls">
        <%= f.file_field :avatar %>
      </div>
    </div>

### Adding fields to the user show action

Create a file in your app at `app/views/admin/users/_show_additions.html.erb`.

    <% if @user.avatar_file_name %>
      <dt>Avatar</dt>
      <dd>
        <%= image_tag @user.avatar.url(:thumb) %>
      </dd>
    <% end %>

Error Handling
------------

The base `TbCore::ApplicationController` will automatically rescue from any `TbCore::NotFoundError` and `TbCore::AccessDeniedError` errors by rendering out the `layouts/error_page.html` template. If you ran the `spud:setup` generator, a template of that name will have been created for you automatically.

When building your own apps you may raise a `TbCore::NotFoundError` or `TbCore::AccessDeniedError` error at any time to halt further execution and cause the error page to render. For example:

    class CarsController
      before_action :get_record
      # ... actions here
      def get_record
        @car = Car.find_by(:id => params[:id])
        if @car.blank?
          raise Spud::NotFoundError.new('car')
        end
      end
    end

Error messages also support localization. Add the following to `en.yml`, as well as any other locales you are using:

    en:
      tb_core:
        errors:
          not_found:
            title: "Not Found"
            message: "The %{item} you were looking for could not be found."
          access_denied:
            title: "Access Denied"
            message: "You are not authorized to view the requested %{item}."

Form Builder
------------

The `TbCore::FormBuilder` class is designed to output a Bootstrap form in the `form-horizontal` style. Use it to quickly build simple forms with lots of fields. Validation errors will appear automatically next to their input fields.

You can specificy the `:builder` attribute in a `form_for`, or you can just use the provided `tb_form_for` helper to use it automatically.

### Example

    <%= tb_form_for @widget do |f| %>
      <%= f.tb_text_field :name %>
      <%= f.tb_number_field :age %>
      <%= f.tb_save_buttons('Widget', widgets_path) %>
    <% end %>

See `form_builder.rb` for the full details.

### User Select

A common case is to have a model in your app that `belongs_to :spud_user`. The `tb_user_select` form helper method makes it easy to assign or create new users from within another form.

    <%= tb_form_for @widget do |f| %>
      <%= f.tb_user_select %>
      <%= f.tb_text_field :name %>
      <%= f.tb_save_buttons('Widget', widgets_path) %>
    <% end %>

Remote Forms
--------------

Rails-validator is included for easy remote form validation.

**Example**

    <%= tb_form_for @widget, remote: true, data: { errors: :inline, success: widgets_path } do |f| %>
      <!--input fields here -->
    <% end %>

See [rails-validator](https://github.com/moser-inc/rails-validator) for details.

Roles & Permissions
-------------------

A user can be assigned to a role, which itself has one or more permissions. These permissions serve two distinct purposes: Determining what dashboard apps a user has access to (if any), and being used within your application logic to establish fine-grained user abilities.

Checking permissions is easy. Each permission has a `tag` which is a unique, namespaced identifier. Use the `permission?` and `any_permission?` methods on the user object to check for their existence. Keep in mind that these calls will always return true for users who have the `super_admin` flag set.

    if !@spud_user.permission?('my_website.clients.my_clients')
      redirect_to root_path, :notice => 'Permission Denied!'
    end

Permissions are created one of two ways:

1. Every dashboard app automatically generates a "Full Access" permission, with a tag of `admin.(app name).full_access`.
2. You as the developer may append custom permissions to the `TbCore.permissions` array.

Create custom permissions whenever you need to permit an action that falls outside of the standard "full access to an app" use case; For example, turning on/off the ability for a user to upload an avatar.

    // application.rb
    TbCore.permissions += [
      {:tag => 'my_website.profile.avatar', :name => 'Upload an avatar to my profile'}
    ]
    // some_view.html.erb
    <% if current_user.permission?('my_website.profile.avatar') %>
      <%= link_to 'Upload Avatar', upload_avatar_path %>
    <% end %>

Finally, custom permissions may optionally be tied to one or more dashboard apps. A user who has the permission shown below would have access to the the Clients and Projects dashboard apps. After that is is up to you to code your view and controller logic in accorance to what permissions the user has.

    // application.rb
    TbCore.permissions += [{
      :tag => 'my_website.projects.project_management',
      :name => 'Manage clients and projects, but cannot delete them or view private info',
      :apps => [:clients, :projects]
    }]

Testing
-----------------

Twice Baked uses RSpec for testing. Get the tests running with a few short commands:

1. Create and migrate the databases:

        rake db:create
        rake db:migrate

2. Load the schema in to the test database:

        rake app:db:test:prepare

3. Run the tests with RSpec

        rspec spec

After the tests have completed the current code coverage stats is available by opening `/coverage/index.html` in a browser.

You can include a few basic test helpers in your own twice baked applications by requiring `tb_core/test_helper` in your specs.
