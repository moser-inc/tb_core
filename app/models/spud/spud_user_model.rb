class Spud::SpudUserModel < ActiveRecord::Base
  self.abstract_class = true
  include TbCore::UserModel

  def initialize(*args)
    ActiveSupport::Deprecation.warn(
      'Spud::SpudUserModel has been replaced by TbCore::UserModel. See the README for details.',
      caller
    )
    super
  end
end
