module TbCore::ApplicationHelper

  def tb_form_for(record, options = {}, &block)
    options[:builder] = TbCore::FormBuilder

    options[:html] ||= {}
    if options[:html][:class]
      options[:html][:class] += ' form-horizontal'
    else
      options[:html][:class] = 'form-horizontal'
    end

    return form_for(record, options, &block)
  end

  def tb_form_with(record, **options, &block)
    options[:builder] = TbCore::FormBuilder

    options[:html] ||= {}
    if options[:html][:class]
      options[:html][:class] += ' form-horizontal'
    else
      options[:html][:class] = 'form-horizontal'
    end

    options[:model] = record

    return form_with(options, &block)
  end

  def tb_form_errors(record, *fields_to_display)
    if record.errors.any?
      content_tag :div, class: 'form-errors test' do
        concat(
          tb_form_error_header(record) +
          content_tag(:ul) do
            if fields_to_display.any?
              messages = fields_to_display.collect{ |field| record.errors.full_messages_for(field) }.flatten()
            else
              messages = record.errors.full_messages
            end
            messages.each do |msg|
              concat content_tag(:li, msg)
            end
          end
        )
      end
    end
  end

  def tb_form_error_header(record)
    if record.errors.any?
      message = "Please correct the following #{'error'.pluralize(record.errors.size)}:"
      return content_tag :h4, message, class: 'form-field-error'
    end
  end

  def tb_form_error_field(record, attribute)
    message = record.errors[attribute].first
    if message
      return content_tag :p, message, class: 'help-block form-field-error'
    end
  end

  def tb_form_field(attribute)
    content_tag :div, class: 'form-group' do
      concat label_tag attribute, attribute, class: 'col-sm-2 control-label'
      concat(content_tag(:div, class: 'col-sm-10') do
        text_field_tag attribute, placeholder: attribute, class: 'form-control'
      end)
    end
  end

  # rubocop:disable Rails/HelperInstanceVariable
  def tb_page_title
    if content_for?(:title)
      title = content_for(:title) + ' | ' + TbCore.site_name
    elsif @page_title
      title = @page_title + ' | ' + TbCore.site_name
    else
      title = TbCore.site_name
    end
    return content_tag :title, title
  end
  # rubocop:enable Rails/HelperInstanceVariable

  def current_site_name
    return TbCore.config.site_name
  end

  def cache_key_for_spud_collection(collection, key:'view', cache_params:[], for_user:false)
    cache_keys = [controller_name, action_name, key]
    cache_keys << collection.collect(&:updated_at).max().try(:utc).try(:to_i)
    if for_user
      cache_keys << current_user_id
    end
    if cache_params.any?
      cache_keys += cache_params.collect{ |cache_param| params[cache_param] || 'nil' }
    end
    cache_keys += collection.collect(&:id)
    cache_key = cache_keys.join('/')
    if cache_key.length > 250
      return Digest::SHA1.hexdigest(cache_key)
    else
      return cache_key
    end
  end

end
