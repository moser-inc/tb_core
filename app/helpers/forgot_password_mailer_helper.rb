module ForgotPasswordMailerHelper
  def perishable_token_link_expiration_time_text(user)
    return if user.class.perishable_token_valid_for.blank?

    expiration_time = user.updated_at + user.class.perishable_token_valid_for

    "This link will expire in #{distance_of_time_in_words(Time.current, expiration_time)}."
  end
end
