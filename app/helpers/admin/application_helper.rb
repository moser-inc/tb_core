module Admin::ApplicationHelper

  def timestamp(datetime=nil)
    return 'Never' if datetime.blank?

    content_tag :span,
      determine_fancy_timestmap(datetime),
      title: datetime
  end

  def determine_fancy_timestmap(datetime)
    if Time.now() > datetime # in the past
      if Time.now() - datetime > 604800
        datetime.strftime('%B %d, %Y') + ' at ' + datetime.strftime('%I:%M %p')
      else
        time_ago_in_words(datetime) + ' ago'
      end
    elsif Time.now() < datetime # in the future
      datetime.strftime('%B %d, %Y') + ' at ' + datetime.strftime('%I:%M %p')
    else
      'Right now'
    end
  end

  def url_for_admin_dashboard_application(url)
    if Rails.configuration.relative_url_root.blank?
      return url
    else
      return [Rails.configuration.relative_url_root, url].join('/').gsub(/(\/+)/, '/')
    end
  end

  # Build an icon with left padding to indicate nesting below the previous table row
  #
  def table_nesting_indicator(depth)
    if depth.positive?
      depth.times do
        concat content_tag(:span, '', class: 'nesting-spacer')
      end
      content_tag(:span, '', class: 'glyphicon glyphicon-chevron-right')
    end
  end

  # The following params will automatically be white listed for tabbed navigation
  #
  WHITE_LIST_PARAMS = [:tab, :sort, :direction, :dir, :search, :id].freeze

  # Build a Bootstrap nav-tabs element
  #
  # * url_helper: A symbol representing the url helper method. ie: admin_widgets_path
  # * tabs: An array of tab hashes with :title and :value keys
  # * white_list: An array of param keys that should be allowed in the tabs. Optional.
  #
  # Example:
  #
  #  <%= tb_core_tabbed_navigation(:admin_vehicles_path, [
  #    {:title => 'All'},
  #    {:title => 'New', :value => 'new'},
  #    {:title => 'Used', :value => 'used'}
  #  ], white_list: [:category_id]) %>
  #
  # This would generate:
  #
  # <ul class="nav nav-tabs">
  #   <li class="active"><a href="/admin/vehicles">All</a></li>
  #   <li class=""><a href="/admin/vehicles?tab=new">New</a></li>
  #   <li class=""><a href="/admin/vehicles?tab=used">Used</a></li>
  # </ul>
  #
  def tb_core_tabbed_navigation(url_helper, tabs, white_list: [])
    key = :tab
    content_tag :ul, class: 'nav nav-tabs' do
      tabs.each do |tab|
        cls = params[key] == tab[:value] ? 'active' : ''
        url = tab.delete(:url)
        if url.blank?
          link_args = params.permit(white_list.concat(WHITE_LIST_PARAMS)).merge(key => tab[:value])
          url = self.send(url_helper, link_args)
        end
        concat(content_tag(:li, class: cls){ link_to tab[:title], url })
      end
    end
  end

end
