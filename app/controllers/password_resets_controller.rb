class PasswordResetsController < ApplicationController

  before_action :load_user_using_perishable_token, only: [:show, :update]
  skip_before_action :require_user, raise: false
  respond_to :html
  layout 'user_sessions'

  def index

  end

  def create
    @user = SpudUser.find_by(email: params[:email])
    if @user
      @user.reset_perishable_token!
      TbCoreMailer.forgot_password_notification(@user, password_reset_url(@user.perishable_token)).deliver_later
      flash[:notice] = 'Password reset instructions have been sent to your email'
      redirect_to login_path
    else
      flash.now[:error] = 'No user was found with that email address'
      render 'index'
    end
  end

  def show

  end

  def update
    if params[:spud_user][:password].blank?
      @user.errors.add(:password, 'must not be blank')
    else
      @user.password = params[:spud_user][:password]
      @user.password_confirmation = params[:spud_user][:password_confirmation]
      if @user.save()
        SpudUserSession.create(@user)
        flash[:notice] = 'Password successfully updated'
        redirect_back_or_default(root_path)
        return
      end
    end
    render 'show'
  end

private

  def load_user_using_perishable_token
    @user = SpudUser.find_using_perishable_token(params[:id])
    unless @user
      flash[:error] = 'Password reset token was invalid or expired'
      redirect_to login_path
    end
  end

end
