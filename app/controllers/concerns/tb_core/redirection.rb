module TbCore
  module Redirection
    extend ActiveSupport::Concern

    included do
      helper_method :back_or_default
    end

    def redirect_back_or_default(default)
      redirect_to(back_or_default(default))
    end

    def back_or_default(default = '/')
      if params[:return_to]
        uri = URI.parse(params[:return_to].to_s)
        return "#{uri.path}?#{uri.query}" if uri.query

        return uri.path
      end
      default
    end

  end
end
