module TbCore
  module UserAuthentication
    extend ActiveSupport::Concern

    included do
      helper_method :current_user_session, :current_user, :current_user_id
      before_action :check_requires_password_change
      around_action :set_time_zone
    end

    def current_user_session
      return @current_user_session if defined?(@current_user_session)

      @current_user_session = SpudUserSession.find
    end

    def current_user
      return @current_user if defined?(@current_user)

      @current_user = current_user_session&.spud_user
    end

    def current_user_id
      return 0 unless @current_user

      @current_user.id
    end

    def require_user
      raise UnauthorizedError.new unless current_user

      true
    end

    def require_admin_user
      raise UnauthorizedError.new unless current_user
      raise AccessDeniedError.new unless current_user.admin_rights?

      true
    end

    def check_requires_password_change
      if current_user.present? && current_user.requires_password_change?
        redirect_to(login_change_password_path(return_to: request.path))
        return false
      end
    end

    def set_time_zone
      old_time_zone = Time.zone
      Time.zone = current_user.time_zone if current_user&.time_zone.present?
      yield
    ensure
      Time.zone = old_time_zone
    end

    def login_path_for_require_user
      login_path(return_to: request.fullpath)
    end

  end
end
