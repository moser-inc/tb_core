class UserSessionsController < ApplicationController

  skip_before_action :require_user, only: [:new, :create, :destroy], raise: false
  skip_before_action :check_requires_password_change,
    only: [:destroy, :change_password, :set_change_password],
    raise: false

  respond_to :html, :json, :js
  layout 'user_sessions'

  def new
    @user_session = SpudUserSession.new
    render 'new'
  end

  def create
    @user_session = SpudUserSession.new(user_session_params.to_h)
    if @user_session.save()
      respond_with @user_session do |format|
        format.html{
          flash[:notice] = 'Login successful!'
          redirect_back_or_default('/')
        }
        format.json{
          render json: {success: true}
        }
      end
    else
      respond_with @user_session do |format|
        format.html{
          render 'new'
        }
      end
    end
  end

  def destroy
    current_user_session.destroy if current_user_session.present?
    respond_with({}) do |format|
      format.html{
        flash[:notice] = 'Logout successful!'
        redirect_back_or_default(login_path)
      }
    end
  end

  def change_password
    require_user
    render 'change_password'
  end

  def set_change_password
    current_user.update(change_password_params)
    respond_with current_user do |format|
      format.html{
        if current_user.errors.any?
          render 'change_password'
        else
          redirect_back_or_default('/')
        end
      }
    end
  end

private

  def user_session_params
    params.require(:spud_user_session).permit(:login, :password, :email)
  end

  def change_password_params
    params.require(:spud_user).permit(:password, :password_confirmation)
  end

end
