class Admin::PasswordResetsController < Admin::ApplicationController

  before_action :load_user_using_perishable_token, only: [:show, :update]
  skip_before_action :require_user, :require_admin_user, raise: false
  layout 'admin/login'

  def index

  end

  def create
    @user = SpudUser.find_by(email: params[:email])
    if @user
      @user.reset_perishable_token!
      TbCoreMailer.forgot_password_notification(@user, admin_password_reset_url(@user.perishable_token)).deliver_later
      flash[:notice] = 'Instructions to reset your password have been emailed to you. ' +
      'Please check your email.'
      redirect_to admin_login_path
    else
      flash.now[:error] = 'No user was found with that email address'
      render 'index'
    end
  end

  def show

  end

  def update
    @user.password = params[:spud_user][:password]
    @user.password_confirmation = params[:spud_user][:password_confirmation]
    if @user.save
      SpudUserSession.create(@user)
      flash[:notice] = 'Password successfully updated'
      redirect_to admin_login_path
    else
      render 'show'
    end
  end

private

  def load_user_using_perishable_token
    @user = SpudUser.find_using_perishable_token(params[:id])
    return if @user

    flash[:notice] = "We're sorry, but we could not locate your account. " +
    'If you are having issues try copying and pasting the URL ' +
    'from your email into your browser or restarting the ' +
    'reset password process.'
    redirect_to admin_login_path
  end
end
