class Admin::RolesController < Admin::ApplicationController

  before_action :get_record, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json, :xml
  belongs_to_app :users
  add_breadcrumb 'Users', :admin_users_path
  add_breadcrumb 'Roles', :admin_roles_path

  def index
    @roles = SpudRole.includes(:spud_role_permissions)
    respond_with @roles
  end
  
  def show
    respond_with @role
  end
  
  def new
    @role = SpudRole.new
    respond_with @role
  end
  
  def create
    logger.debug 'role_params:'
    logger.debug role_params
    @role = SpudRole.new(role_params)
    flash[:notice] = 'SpudRole created successfully' if @role.save
    respond_with @role, location: admin_roles_path
  end
  
  def edit
    respond_with @role
  end
  
  def update
    #role_params[:permission_tags] ||= []
    flash[:notice] = 'SpudRole updated successfully' if @role.update(role_params)
    respond_with @role, location: admin_roles_path
  end
  
  def destroy
    flash[:notice] = 'SpudRole deleted successfully' if @role.destroy
    respond_with @role, location: admin_roles_path
  end

  private

  def get_record
    begin
      @role = SpudRole.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      flash[:error] = 'Could not find the requested SpudRole'
      redirect_to admin_roles_path
      return false
    end
  end

  def role_params
    params.require(:spud_role).permit(:name, permission_tags: [])
  end

end
