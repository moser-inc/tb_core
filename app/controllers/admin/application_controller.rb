class Admin::ApplicationController < TbCore::ApplicationController
  include TbCore::BelongsToApp
  before_action :require_admin_user
  add_breadcrumb 'Dashboard', :admin_root_path
  layout 'admin/detail'
  respond_to :html, :json

private

  def template_for_request_error
    return '/layouts/admin/error_page'
  end

  def login_path_for_require_user
    admin_login_path(return_to: request.path)
  end

end
