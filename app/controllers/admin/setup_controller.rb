class Admin::SetupController < Admin::ApplicationController

  skip_before_action :require_admin_user

  def new
    if SpudUser.count.nonzero?
      flash[:error] = 'Access Denied! This wizard may only be executed when the database is empty.'
      redirect_to admin_login_path and return
    else
      @spud_user = SpudUser.new
    end
  end

  def create
    @spud_user = SpudUser.new(user_params)
    @spud_user.super_admin = true
    if @spud_user.save
      SpudUserSession.create(@spud_user)
      redirect_to admin_root_path
    else
      render 'new', status: :unprocessable_entity
    end
  end

private

  def user_params
    params.require(:spud_user).permit(:login, :email, :password, :password_confirmation, :first_name, :last_name)
  end

end
