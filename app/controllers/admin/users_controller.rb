class Admin::UsersController < Admin::ApplicationController
  require 'csv'

  belongs_to_app :users
  add_breadcrumb 'Users', :admin_users_path
  before_action :load_user, only: [:edit, :update, :show, :destroy]
  after_action :send_credentials_email, only: [:create, :update]
  respond_to :html, :csv

  sortable_by :email, :current_login_at,
    name: [:last_name, :first_name],
    default: :email

  def index
    @spud_users = SpudUser.order(sortable_query).paginate(page: params[:page], per_page: 15)
    if params[:search]
      @spud_users = @spud_users.where_name_like(params[:search])
    end
    respond_with @spud_users
  end

  def show
    respond_with @user
  end

  def export
    users = SpudUser.all
    export_columns = %w[created_at first_name last_name email last_login_at]
    file_name = TbCore.site_name + ' Users Export.csv'
    send_data users.as_csv(export_columns),
      type: 'text/csv; charset=iso-8859-1; header=present',
      disposition: "attachment; filename=#{file_name}"
  end

  def new
    @user = SpudUser.new
    respond_with @user do |format|
      format.html{
        if request.xhr?
          render 'new', layout: false
        else
          render 'new'
        end
      }
    end
  end

  def create
    @user = SpudUser.create(user_params)
    respond_with @user, location: admin_users_path
  end

  def edit
    respond_with @user do |format|
      format.html{
        if request.xhr?
          render 'edit', layout: false
        else
          render 'edit'
        end
      }
    end
  end

  def update
    if @user.update(user_params)
      if @user == current_user && user_params[:password].present?
        SpudUserSession.create(@user)
      end
    end
    respond_with @user, location: admin_user_path(@user), status: 200
  end

  def destroy
    @user.destroy
    respond_with @user, location: admin_users_path do |format|
      format.js{
        render nothing: true, status: :ok
      }
    end
  end

private

  def load_user
    @user = SpudUser.find_by!(id: params[:id])
  end

  def user_params
    params.require(:spud_user).permit!
  end

  def send_credentials_email
    if params[:send_email] && user_params[:password] && @user.errors.none?
      TbCoreMailer.user_credentials(@user, user_params[:password]).deliver_later
    end
    return true
  end

end
