class Admin::SettingsController < Admin::ApplicationController

  before_action :require_user
  skip_before_action :require_admin_user

  before_action do |c|
    if current_user&.admin_rights?
      add_breadcrumb 'Settings', :admin_settings_path
    end
  end

  def edit
  end

  def update
    if check_password && @current_user.update(user_params)
      if user_params.include?(:password)
        SpudUserSession.create(@current_user)
      end
      flash[:notice] = 'User settings saved successfully.'
      respond_with @current_user, location: admin_settings_path
    else
      render 'edit', status: :unauthorized
    end
  end

private

  def user_params
    params.require(:spud_user).permit(
      :login, :first_name, :last_name, :email,
      :password, :password_confirmation, :time_zone
    )
  end

  def check_password
    if user_params[:password].blank?
      return true
    else
      if !current_user.valid_password?(params[:current_password])
        current_user.errors.add(:current_password, 'is not correct. Please enter correct password.')
        return false
      end
      return true
    end
  end

end
