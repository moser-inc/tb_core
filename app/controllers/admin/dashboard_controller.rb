class Admin::DashboardController < Admin::ApplicationController

  respond_to :html, :json
  layout 'admin/application'

  def index
    @setting = SpudUserSetting.find_by(spud_user_id: current_user.id, key: 'dash_icon_order')
    apps = TbCore.admin_applications
    if @setting.nil? == false
      apps = arrange_apps(apps)
    end
    @admin_applications = apps.select do |admin_application|
      current_user.can_view_app?(admin_application)
    end
  end

  def change_sort
    @order = params[:order]
    @setting = SpudUserSetting.find_or_initialize_by(spud_user_id: current_user.id, key: 'dash_icon_order')
    @setting.value = @order
    if @setting.save
      respond_to do |format|
        format.json  { head :no_content }
      end
    end
  end

  def badges
    data = []
    @admin_applications = TbCore.admin_applications.select do |admin_application|
      if current_user.can_view_app?(admin_application)
        if admin_application[:badge]
          data << {key: admin_application[:key], badge_count: admin_application[:badge].call(current_user)}
        end
      end
    end
    render json: {data: data}
  end

  private

  def arrange_apps(applications)
    begin
      order = JSON.parse(@setting.value)
    rescue JSON::ParserError
      return applications
    end

    return applications.sort do |a, b|
      index_a = order.index(a[:url])
      index_b = order.index(b[:url])
      if index_a.nil?
        1
      elsif index_b.nil?
        -1
      else
        index_a <=> index_b
      end
    end
  end
end
