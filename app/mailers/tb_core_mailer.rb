class TbCoreMailer < ActionMailer::Base
  helper ForgotPasswordMailerHelper

  default from: TbCore.from_address
  layout 'mailer'

  def forgot_password_notification(user, url)
    @user = user
    @url = url
    mail(
      to: to_address_for_user(user),
      subject: default_i18n_subject(site_name: TbCore.site_name)
    )
  end

  def user_credentials(user, password)
    @user = user
    @password = password
    mail(
      to: to_address_for_user(user),
      subject: default_i18n_subject(site_name: TbCore.site_name)
    )
  end

private

  def to_address_for_user(user)
    address = Mail::Address.new(user.email)
    address.display_name = user.full_name
    return address.format
  end

end
