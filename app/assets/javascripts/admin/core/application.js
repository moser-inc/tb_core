//= require rails-ujs
//= require rails-validator
//= require jquery
//= require bootstrap-sprockets
//= require_self
//= require_tree .

window.tb = {};

// for legacy code
window.spud = { admin: window.tb };

document.addEventListener('DOMContentLoaded', function() {

  if (typeof tinymce != "undefined") {
    tb.editor.init();
  }

  $("#modal_window .modal-footer .form-submit").bind('click', function() {
    $("#modal_window .modal-body form").submit();
  });

  $("#modal_window ").on('hidden', function(){
    $(this).find('.modal-footer-additional').remove();
    $(this).find('.modal-footer-default').show();
    $(this).removeData('modal');
  });

  $('body').on('click', 'a.ajax', function() {
    var url = this.href;
    var title = this.title;
    var dialog = $("#modal_window");

    $("#modal_window .modal-title").text(title);
    dialog.modal({
      remote: url +".js",
      show:true
    });
    return false;
  });

});
