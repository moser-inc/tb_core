(function(){

var $picker;

tb.users = {
  init: function(){
    var $body = $('body');
    $body.on('click', '.btn-generate-password', clickedGeneratePassword);
    $body.on('change', '#spud_user_password', changedUserPassword);

    $picker = $('.tb-user-select');
    if($picker.length > 0){
      $picker.on('change', changedUserSelect).trigger('change');
      $body.on('click', '.tb-user-select-edit', clickedPickerEdit);
      $body.on('click', '.tb-user-select-add', clickedPickerAdd);
      $('.modal-body').on('ajax:success', '.user-form', savedModalUser);
    }
  }
};

/*
* Set a randomly generated password
*/
var clickedGeneratePassword = function(e){
  e.preventDefault();
  var randomPassword = generatePassword(12);
  $('#spud_user_password').val(randomPassword).trigger('change');
  $('#spud_user_password_confirmation').val(randomPassword);
  $('.generated-password').text("Generated: " + randomPassword);
  $('#spud_user_requires_password_change').prop('checked', true);
  $('#send_email').prop('checked', true);
};

var generatePassword = function(length){
  var charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  var password = '';
  for(var i=0, n=charset.length; i<length; i++){
    password += charset.charAt(Math.floor(Math.random() * n));
  }
  return password;
};

/*
* If a password is set, display the Send Email option. Otherwise, hide it.
*/
var changedUserPassword = function(e){
  var value = $(this).val();
  if(value === ''){
    $('.user-send-email-group').hide().prop('checked', false);
  }
  else{
    $('.user-send-email-group').show();
  }
};

/*
* If a user is selected, enable the Edit button. Otherwise disable it.
*/
var changedUserSelect = function(e){
  var id = $(this).val();
  var $edit = $('.tb-user-select-edit');
  if(id){
    $('.tb-user-select-edit').attr('disabled', false);
  }
  else{
    $('.tb-user-select-edit').attr('disabled', true);
  }
};

/*
* Bring up the edit form when the Edit button is clicked
*/
var clickedPickerEdit = function(e){
  e.preventDefault();
  var id = $('.tb-user-select').val();
  if(id){
    var href = $(this).attr('href').replace(':id', id);
    $.ajax(href, {
      dataType: 'html',
      success: function(html){
        tb.modal.displayWithOptions({
          title: 'Edit User',
          html: html,
          hideFooter: true
        });
      }
    });
  }
};

/*
* Bring up the new user form when the Add button is clicked
*/
var clickedPickerAdd = function(e){
  e.preventDefault();
  $.ajax($(this).attr('href'), {
    dataType: 'html',
    success: function(html){
      tb.modal.displayWithOptions({
        title: 'New User',
        html: html,
        hideFooter: true
      });
    }
  });
};

/*
* When a user is saved, close the modal and update the picker
*/
var savedModalUser = function(event){
  var json = event.originalEvent.detail[0]
  var $option = $('<option/>', {
    value: json.id,
    text: json.first_name + ' ' + json.last_name + ' (' + json.email + ')'
  });
  var $existingOption = $('.tb-user-select option[value=' + json.id + ']');
  if($existingOption.length > 0){
    $existingOption.replaceWith($option);
  }
  else{
    $picker.append($option);
  }
  $picker.val(json.id);
  tb.modal.hide();
  return false;
};

document.addEventListener('DOMContentLoaded', tb.users.init);

})();
