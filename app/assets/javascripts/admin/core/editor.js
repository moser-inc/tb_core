tb.editor = {};

(function(){

  var editor = tb.editor;

  /*
  * Use tb.editor.overrides to set custom configs in your application
  *
  * tb.editor.overrides = { menubar: true }
  *
  */
  editor.overrides = {};

  var registeredPlugins = [
    'autolink','lists','image','link','media','paste', 'code'
  ];

  var registeredButtons = [
    'undo', 'redo', '|',
    'bold','italic','underline','strikethrough','|',
    'formatselect','|',
    'cut','copy','paste', '|',
    'bullist','numlist','outdent','indent','|',
    'link','unlink','image', '|', 'code', '|'
  ];

  var extendedValidElements = [
    "iframe[src|width|height|name|align|frameborder|allowfullscreen]"
  ];

  editor.init = function(args) {
    editor.unload();

    var selector = args ? args.selector : 'textarea.tinymce';

    var options = Object.assign({
      selector: selector,
      height: 400,
      menubar: false,
      toolbar: registeredButtons.join(' '),
      plugins: registeredPlugins.join(' '),
      extended_valid_elements: extendedValidElements.join(","),
      convert_urls: false
    }, editor.overrides);

    tinymce.init(options)
  };

  editor.unload = function() {
    if(typeof tinymce != "undefined"){
      tinyMCE.remove();
    }
  };

  editor.registerPlugin = function(pluginName){
    if($.inArray(registeredPlugins, pluginName) < 0){
      registeredPlugins.push(pluginName);
    }
  };

  editor.deregisterPlugin = function(pluginName){
    var i=0;
    while(i < registeredPlugins.length){
      if(registeredPlugins[i] == pluginName){
        registeredPlugins.splice(1, i);
        break;
      }
      i++;
    }
  };

  editor.appendValidElement = function(element) {
    extendedValidElements.push(element);
  };

  editor.registerButton = function(buttonNameOrArray, rowNum){
    if(rowNum){
      console.warn('rowNum parameter is no longer used.');
    }
    if(typeof(buttonNameOrArray) == 'object'){
      registeredButtons = registeredButtons.concat(buttonNameOrArray);
    }
    else{
      registeredButtons.push(buttonNameOrArray);
    }
  };

})();
