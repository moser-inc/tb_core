(function(){

tb.modal = {

  displayWithOptions: function(options){
    var modal = $('#modal_window');
    if (options.title){
      modal.find('.modal-title').text(options.title);
    }
    if (options.html){
      modal.find('.modal-body').html(options.html);
    }
    if (options.size) {
      var sizeClass = '';
      switch (options.size) {
        case "lg": //because some people like brevity
        case "large": //some like accuracy
        case "modal-lg": // some like bootstrap
          sizeClass = "modal-lg";
          break;
        case "sm":
        case "small":
        case "modal-sm":
          sizeClass = "modal-sm";
          break;
      }
      modal.find('.modal-dialog').addClass(sizeClass);
    }
    var defaultFooter = modal.find('.modal-footer-default');
    if(options.buttons){
      var newFooter = defaultFooter.clone();
      newFooter.removeClass('modal-footer-default').addClass('modal-footer-additional');
      newFooter.find('.form-submit').remove();
      for(var key in options.buttons){
        newFooter.append('<button class="btn btn-default '+key+'">'+options.buttons[key]+'</button>');
      }
      defaultFooter.hide();
      newFooter.insertAfter(defaultFooter);
    } else {
      $('modal-footer-additional').remove();
      if(options.hideFooter){
        defaultFooter.hide();
      } else{
        defaultFooter.show();  
      }
    }

    // modalArgs are passed straight to the modal and can contain bootstrap default properties.
    var modalArgs = 'show';
    if (options.disableClose) {
      // This modal cannot be closed by pressing escape or clicking the background page.
      modalArgs = {
        backdrop: 'static',
        keyboard: false
      };
    }

    modal.modal(modalArgs);
  },

  hide: function(){
    var modal = $('#modal_window');
    modal.modal('hide');
  }

};

})();
