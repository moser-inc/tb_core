//= require sortable/sortable

(function(){

var badgeInterval;

tb.dashboard = {
  init:function(){
    badgeInterval = setInterval(updateBadges, 30000);
    updateBadges();
    sortableIcons();
  }
};

var updateBadges = function() {
  $.ajax({
    url: '/admin/badges'
  }).always(function(json) {
    if(json && json.data) {
      if(json.data.length === 0){
        clearInterval(badgeInterval);
      }
      else{
        json.data.forEach(function(eachBadge) {
          var id = "application_name_" + eachBadge.key;
          updateBadge(id, eachBadge.badge_count);
        });
      }
    }
  });
};

var updateBadge = function(badge_id, count) {
  var badgeIcon = $('#' + badge_id).find('.dash-icon-badge');
  if(badgeIcon.length === 0) {
    return;
  }
  else if (count === 0) {
    badgeIcon.toggle(false);
  }
  else if (count > 999) {
    badgeIcon.text("999+").toggle(true);
  }
  else {
    badgeIcon.text(count).toggle(true);
  }
};

var sortableIcons = function(){
  var element = document.querySelector('.sortable');
  var sortable = Sortable.create(element, {
    onUpdate: function() {
      var sortArr = [];
      var index = 0;
      document.querySelectorAll('.sortable > div > a').forEach(function(element){
        sortArr.push(element.getAttribute('href'));
      });
      saveOrder(sortArr);
    }
  });
};

function saveOrder(sortArray) {
  $.ajax('/admin/change_sort', {
    method: 'PUT',
    data: { order: sortArray },
    dataType: "json",
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      console.error('Saving sort order failed:', arguments);
    }
  });
}

})();
