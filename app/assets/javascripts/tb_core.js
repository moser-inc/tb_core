//= require rails-ujs
//= require rails-validator

console.warn("You are including tb_core.js in your application; This file is deprecated and may be removed.\n\
For similar functionality, replace tb_core in your asset pipeline with the following:\n\
\n\
//= require rails-ujs\n\
//= require rails-validator")
