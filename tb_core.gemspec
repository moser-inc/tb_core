$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'tb_core/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'tb_core'
  s.version     = TbCore::VERSION
  s.authors     = ['Greg Woods']
  s.email       = ['greg.woods@moserit.com']
  s.homepage    = 'http://bitbucket.org/moser-inc/tb_core'
  s.summary     = 'Twice Baked Core Engine'
  s.description = 'Twice Baked Core Engine'

  s.files = Dir['{app,config,db,lib,vendor}/**/*'] + ['MIT-LICENSE', 'Rakefile', 'README.md']
  s.test_files = Dir.glob('spec/**/*').reject{ |f| f.match(/^spec\/dummy\/(log|tmp)/) }

  s.required_ruby_version = '>= 2.4.2'
  s.add_dependency 'authlogic', '>= 5.0'
  s.add_dependency 'bootstrap-sass', '< 4.0'
  s.add_dependency 'breadcrumbs_on_rails'
  s.add_dependency 'jquery-rails' # remove when possible
  s.add_dependency 'rails', '>= 5.0'
  s.add_dependency 'rails-ujs'
  s.add_dependency 'responders', '>= 2.2'
  s.add_dependency 'sass-rails', '>= 5.0'
  s.add_dependency 'scrypt'
  s.add_dependency 'sortable_by'
  s.add_dependency 'tinymce-rails'
  s.add_dependency 'will_paginate', '>= 3.1'
  s.add_dependency 'will_paginate-bootstrap', '>= 1.0.1'

  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'factory_bot_rails'
  s.add_development_dependency 'pg', '>= 0.15'
  s.add_development_dependency 'rails-controller-testing'
  s.add_development_dependency 'rspec-rails', '>= 4.0.0.beta2'
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'rubocop-performance'
  s.add_development_dependency 'rubocop-rails'
  s.add_development_dependency 'simplecov'
end
